package org.assignments;


public class SortingApp {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.print("No elements were provided.");
            return;
        } else if (args.length == 1) {
            System.out.print("Only one element was provided. ");
            System.out.print(Integer.parseInt(args[0]));
            return;
        } else if (args.length > 10) {
            System.out.print("Application is only sorting up to ten elements.");
            return;
        }
        int[] array = inputArray(args);
        Sorting.sort(array);
        printArray(array);
    }

    private static int[] inputArray(String[] args){
        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }
        return numbers;
    }

    private static void printArray(int[] array) {
        for (int element : array) {
            System.out.print(element + " ");
        }
    }
}