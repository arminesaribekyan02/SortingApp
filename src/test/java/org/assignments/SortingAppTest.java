package org.assignments;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class SortingAppTest extends TestCase {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private String[] args;
    private String expected;

    public SortingAppTest(String[] args, String expected) {
        this.args = args;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[] {}, "No elements were provided."},
                {new String[] {"5"}, "Only one element was provided. " + "5"},
                {new String[] {"1", "5", "8", "9", "6", "0"}, "0 1 5 6 8 9 "},
                {new String[] {"10", "8", "5", "6", "7", "8", "9", "11", "2", "1", "15", "4"}, "Application is only sorting up to ten elements."}
        });
    }

    @Test
    public void testStart() {
        System.setOut(new PrintStream(outContent));
        SortingApp.main(args);
        assertEquals(expected, outContent.toString());
        System.setOut(originalOut);
    }
}